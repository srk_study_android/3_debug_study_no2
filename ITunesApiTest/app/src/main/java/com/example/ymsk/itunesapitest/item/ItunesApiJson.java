package com.example.ymsk.itunesapitest.item;

/**
 * Created by ymsk on 2016/12/24.
 */

public class ItunesApiJson {

    /**
     * JsonArrayのタグ
     */
    public static final String ARRAY_RESULT = "results";

    /**
     * Jsonのタグ
     */
    public static final String TAG_TITLE = "trackCensoredName";
    public static final String TAG_ARTIST = "artistName";
    public static final String TAG_ALBUM = "collectionName";
    public static final String TAG_ALBUM_ART_URL = "artworkUrl100";
    public static final String TAG_PREVIEW_URL = "previewUrl";

    private ItunesApiJson() {
    }
}
