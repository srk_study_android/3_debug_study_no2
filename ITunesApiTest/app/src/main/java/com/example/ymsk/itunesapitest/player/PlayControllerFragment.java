package com.example.ymsk.itunesapitest.player;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.ymsk.itunesapitest.MainFragment;
import com.example.ymsk.itunesapitest.R;
import com.example.ymsk.itunesapitest.common.TextUtil;
import com.example.ymsk.itunesapitest.item.Item;

import java.util.ArrayList;

/**
 * Created by ymsk on 2016/12/23.
 */

public class PlayControllerFragment extends Fragment implements
        View.OnClickListener,
        MusicController.PlayerStateListener {

    public static final String INTENT_PARAM_CURRENT = "intent_param_current_track";
    public static final String INTENT_PARAM_MUSIC_ITEM = "intent_param_music_item";

    public static final String TAG = PlayControllerFragment.class.getSimpleName();

    private ImageView mPlayPauseButton;
    private ImageView mPrevButton;
    private ImageView mNextButton;
    private ImageView mAlbumImage;
    private ImageView mCloseButton;

    private TextView mTitle;
    private TextView mArtist;

    private MusicController mMusicController;
    private PlayControllerStateChangeListener mListener;

    /**
     * PlayControllerFragmentの表示状態を監視するリスナー
     */
    public interface PlayControllerStateChangeListener {
        public void onHidePlayController();
        public void onShowPlayController();
    }

    public void setOnPlayControllerStateChangeListener(PlayControllerStateChangeListener listener) {
        mListener = listener;
    }

    public void PlayControlBottomFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_player_control, container, false);

        Bundle bundle = getArguments();
        final ArrayList<Item> items = bundle.getParcelableArrayList(INTENT_PARAM_MUSIC_ITEM);
        final int current = bundle.getInt(INTENT_PARAM_CURRENT);
        initView(rootView, items, current);

        // PlayControllerFragmentが表示されたことを、リスナー登録者へ通知
        mListener.onShowPlayController();

        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mMusicController.removeListener();
        mMusicController.finish();
        mMusicController = null;

        // PlayControllerFragmentの表示が破棄されたことを、リスナー登録者へ通知
        mListener.onHidePlayController();
    }

    @Override
    public void onClick(View view) {
        Item item = mMusicController.getItem();

        switch (view.getId()) {
            case R.id.play_pause_button:
                onPlayPauseButton(mMusicController.isPlaying());
                break;
            case R.id.prev_button:
                mMusicController.prev();
                break;
            case R.id.next_button:
                mMusicController.next();
                break;
            case R.id.close_button:
                //  Fragment(自分自身)を破棄する。Activityのfinish()に相当
                getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
                break;
            case R.id.play_controller_music_artist:
                TextUtil.doClipCopy(getContext(), item.getArtistName());
                break;
            case R.id.play_controller_music_title:
                TextUtil.doClipCopy(getContext(), item.getTitle());
                break;
            default:
                break;
        }
    }

    /**
     * ================
     * onPlayerStart, onPlayerPause, onContentChanged, onError
     * 以上4つのOverride MethodたちはMusicControllerクラスからプレイヤー関連のコールバックを受け取る
     * ================
     */
    @Override
    public void onPlayerStart() {
        mPlayPauseButton.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_pause_black_24dp));
    }

    @Override
    public void onPlayerPause() {
        mPlayPauseButton.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_play_arrow_black_24dp));
    }

    @Override
    public void onContentChanged() {
        updateScreen(mMusicController.getItem());
    }

    @Override
    public void onPrepare() {
        MainFragment fragment = (MainFragment) getActivity().getSupportFragmentManager().findFragmentByTag(MainFragment.TAG);
        if(fragment != null) {
            Item item = mMusicController.getItem();
            fragment.showLoadingSnackBar(item.getTitle(), item.getArtistName());
        }
    }

    @Override
    public void onError(int what) {
        Toast.makeText(getActivity(), R.string.error_play_failed, Toast.LENGTH_SHORT).show();
    }

    private void initView(View rootView, ArrayList<Item> items, int current) {
        mPlayPauseButton = (ImageView) rootView.findViewById(R.id.play_pause_button);
        mPrevButton = (ImageView) rootView.findViewById(R.id.prev_button);
        mNextButton = (ImageView) rootView.findViewById(R.id.next_button);
        mCloseButton = (ImageView) rootView.findViewById(R.id.close_button);
        mTitle = (TextView) rootView.findViewById(R.id.play_controller_music_title);
        mArtist = (TextView) rootView.findViewById(R.id.play_controller_music_artist);

        mPlayPauseButton.setOnClickListener(this);
        mPrevButton.setOnClickListener(this);
        mNextButton.setOnClickListener(this);
        mCloseButton.setOnClickListener(this);

        mTitle.setOnClickListener(this);
        mArtist.setOnClickListener(this);

        mAlbumImage = (ImageView) rootView.findViewById(R.id.album_image);


        mMusicController = MusicController.newInstance(getActivity(), items, current);
        mMusicController.setListener(this);
        mMusicController.init();
    }

    private void updateScreen(Item item) {

        final String title     = item.getTitle();
        final String artist    = item.getArtistName();
        final String imagePath = item.getImageUrl();

        mTitle.setText(title);
        mArtist.setText(artist);

        Glide.with(this)
                .load(imagePath)
                .centerCrop()
                .into(mAlbumImage);
    }

    private void onPlayPauseButton(boolean isPlaying) {
        if(isPlaying) {
            mMusicController.pause();
        } else {
            mMusicController.start();
        }
    }
}
