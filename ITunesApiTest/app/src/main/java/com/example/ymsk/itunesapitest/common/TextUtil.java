package com.example.ymsk.itunesapitest.common;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.widget.Toast;

/**
 * Created by ymsk on 2016/12/24.
 */

public class TextUtil {

    private final static String LABEL_STR_COPY = "label";

    public static void doClipCopy(Context context, String text) {
        ClipboardManager clipboardManager =(ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clipData = ClipData.newPlainText(LABEL_STR_COPY, text);
        clipboardManager.setPrimaryClip(clipData);
        Toast.makeText(context, "コピーしました : " + text, Toast.LENGTH_SHORT).show();
    }

    private TextUtil() {
    }
}
