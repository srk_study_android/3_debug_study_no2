package com.example.ymsk.itunesapitest.item;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.util.SparseArray;

/**
 * Created by ymsk on 2016/12/14.
 */

public class ItemLoader extends AsyncTaskLoader {

    private final String mSearchUrl;
    private Context mContext;

    public ItemLoader(Context context, String searchText, String reqType) {
        super(context);
        mContext = context;
        mSearchUrl = ItunesUrlCreator.create(searchText, reqType);
    }

    @Override
    public Object loadInBackground() {
        ITunesApiAccessor apiAccessor = new ITunesApiAccessor(mSearchUrl);
        return apiAccessor.getItemList(mContext);
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }

    /**
     * リクエストURLを作るクラス
     */
    public static class ItunesUrlCreator {
        private static final String BASE_HTML = "http://itunes.apple.com/search?";
        private static final String AND = "&amp;";

        private static String create(String searchText, String reqType) {
            return new StringBuilder()
                    .append(BASE_HTML)
                    .append("term=").append(searchText).append(AND)   // 検索キーワード
                    .append("country=").append("JP").append(AND)      // 国
                    .append("lang=").append("ja_jp").append(AND)      // 言語
                    .append("media=").append("music").append(AND)     // 大分類
                    .append("entity=").append("song").append(AND)     // 小分類
                    .append("attribute=").append(reqType).append(AND) // 検索オーダー
                    .append("limit=").append("50").append(AND)        // 一度に取得する検索結果
                    .toString();
        }
    }
}
