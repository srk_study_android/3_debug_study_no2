package com.example.ymsk.itunesapitest.player;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.Log;

import com.example.ymsk.itunesapitest.item.Item;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by ymsk on 2016/12/21.
 */

public class MusicController implements
        MediaPlayer.OnPreparedListener,
        MediaPlayer.OnCompletionListener,
        MediaPlayer.OnErrorListener {

    public static final String TAG = MusicController.class.getSimpleName();

    /**
     * Player状態の変更を通知するリスナー
     */
    public interface PlayerStateListener {
        void onPlayerStart();
        void onPlayerPause();
        void onPrepare();
        void onContentChanged();
        void onError(int what);
    }

    public void setListener(PlayerStateListener listener) {
        mListener = listener;
    }

    public void removeListener() {
        mListener = null;
    }

    public static MusicController newInstance(Context context, ArrayList<Item> items, int current) {
        return new MusicController(context, items, current);
    }

    /**
     * 再生完了時に呼ばれる
     * @param mediaPlayer
     */
    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        // 次の曲へ
        next();
    }

    /**
     * prepareAsync()の処理が完了すると呼ばれる = 再生開始準備完了
     * @param mediaPlayer
     */
    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        // 再生開始
        start();
        mListener.onContentChanged();
    }

    /**
     * エラーハンドリング
     */
    @Override
    public boolean onError(MediaPlayer mediaPlayer, int what, int extra) {
        switch (what) {
            case MediaPlayer.MEDIA_ERROR_UNKNOWN:
                Log.e(TAG, "MEDIA_ERROR_UNKNOWN :" + "what=" + what + "extra=" + extra);
                break;
            case MediaPlayer.MEDIA_ERROR_SERVER_DIED:
                Log.e(TAG, "MEDIA_ERROR_SERVER_DIED :" + "what=" + what + "extra=" + extra);
                break;
        }
        mListener.onError(what);
        return false;
    }

    /**
     * プレイヤーに音楽をインプットする
     */
    public void init() {
        Uri uri = Uri.parse(mState.items.get(mState.position).getPreviewUrl());
        try {
            mPlayer.reset();
            mPlayer.setDataSource(mContext, uri);
        } catch (IOException e) {
            e.printStackTrace();
        }
        mPlayer.prepareAsync();/* Async(非同期)でリソース要求, コールバックはonPrepared() */
        mListener.onPrepare();
    }

    /**
     * プレイヤー破棄時に呼ぶ
     */
    public void finish() {
        mPlayer.stop();
        mPlayer.release();
        mPlayer = null;
    }

    /**
     * 再生開始
     */
    public void start() {
        mPlayer.start();
        mListener.onPlayerStart();
        mState.mNowState = PlayerState.Playback.Playing;
    }

    /**
     * 一時停止
     */
    public void pause() {
        mPlayer.pause();
        mListener.onPlayerPause();
        mState.mNowState = PlayerState.Playback.Pausing;
    }

    /**
     * 曲送り
     */
    public void next() {
        final int last = mState.items.size() - 1;
        if(mState.position == last) {
            mState.position = 0;
        } else {
            mState.position += 1;
        }
        init();
    }

    /**
     * 曲戻し
     */
    public void prev() {
        if(mState.position != 0) {
            mState.position -= 1;
        } else {
            mState.position = mState.items.size() - 1;
        }
        init();
    }

    /**
     * プレイヤー状態取得
     */
    public PlayerState.Playback getState() {
        return mState.mNowState;
    }

    /**
     * 再生中かどうかをbooleanで返す
     */
    public boolean isPlaying() {
        return mState.mNowState.equals(PlayerState.Playback.Playing);
    }

    /**
     * 再生中の楽曲データ(Itemオブジェクト)を返す
     */
    public Item getItem() {
        return mState.items.get(mState.position);
    }

    /**
     * コンストラクタがprivate = シングルトンパターン
     * -> MediaControllerオブジェクトが複数存在することを許容しない
     */
    private MusicController(Context context, ArrayList<Item> items, int current) {
        mContext = context;

        mPlayer = new MediaPlayer();
        mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mPlayer.setOnCompletionListener(this);
        mPlayer.setOnErrorListener(this);
        mPlayer.setOnPreparedListener(this);

        mState = new PlayerState(items, current);
    }

    private Context mContext;
    private MediaPlayer mPlayer;
    private PlayerState mState;

    private PlayerStateListener mListener = null;

    /**
     * プレイヤー状態保持用クラス
     */
    public static class PlayerState {
        public enum Playback {
            Stopping,   // 停止中
            Pausing,    // 一時停止中
            Playing,    // 再生中
        }
        PlayerState( ArrayList<Item> items, int current) {
            this.position = current;
            this.items = items;
        }
        private Playback mNowState = Playback.Stopping;
        private int position = 0;
        private ArrayList<Item> items;
    }
}
