package com.example.ymsk.itunesapitest;

import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.ymsk.itunesapitest.player.PlayControllerFragment;

public class MainActivity extends AppCompatActivity implements PlayControllerFragment.PlayControllerStateChangeListener {

    private View mView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initMainView();
    }

    @Override
    public void onHidePlayController() {
        setPlayerFragmentMargin(0);
    }

    @Override
    public void onShowPlayController() {
        int margin = (int)getResources().getDimension(R.dimen.fragment_play_controller_height);
        setPlayerFragmentMargin(margin);
    }

    /**
     * PlayControllerFragmentを生成する
     */
    public PlayControllerFragment createPlayerFragment() {
        PlayControllerFragment fragment = new PlayControllerFragment();
        fragment.setOnPlayControllerStateChangeListener(this);

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        ft.replace(
                R.id.main_playback_container,
                fragment,
                PlayControllerFragment.TAG
        ).commit();
        View view = findViewById(R.id.main_playback_container);
        view.setVisibility(View.VISIBLE);
        return fragment;
    }

    private void initMainView() {
        FragmentManager fm = getSupportFragmentManager();
        mView = findViewById(R.id.main_container);

        fm.beginTransaction().replace(
                R.id.main_container,
                new MainFragment(),
                MainFragment.TAG
        ).commit();
    }

    /**
     * PlayerFragmentはFrameLayoutのため、
     * プレイヤー表示中はその高さ分だけメインコンテンツの表示領域を覆い隠してしまう。
     * その対策として、このメソッドでメインコンテンツにプレイヤーの高さ分のマージンを与奪する
     */
    private void setPlayerFragmentMargin(int margin) {
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) mView.getLayoutParams();
        params.setMargins(0, 0, 0, margin);
        mView.setLayoutParams(params);
    }
}
