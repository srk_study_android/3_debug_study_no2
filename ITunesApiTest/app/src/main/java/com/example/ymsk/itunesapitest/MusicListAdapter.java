package com.example.ymsk.itunesapitest;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.ymsk.itunesapitest.item.Item;

import java.util.ArrayList;

/**
 * Created by ymsk on 2016/12/14.
 */

public class MusicListAdapter extends RecyclerView.Adapter{

    private ArrayList<Item> mItems;
    private LayoutInflater mInflater;
    private Context mContext;
    private OnRecyclerItemClickListener mClickListener;

    public MusicListAdapter(Context context, ArrayList<Item> items) {
        super();
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mItems = items;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemViewHolder(mInflater.inflate(R.layout.view_recycler_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ItemViewHolder itemViewHolder = (ItemViewHolder) holder;

        final int clickedItemPosition = position;
        final Item current = mItems.get(position);
        final String artistName = current.getArtistName();
        final String title = current.getTitle();
        final String imageUrl = current.getImageUrl();

        final View.OnClickListener listener =  new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // クリック時の処理はリスナーの実装者に移譲する
                mClickListener.onItemClick(view, current, clickedItemPosition);
            }
        };

        itemViewHolder.title.setText(title);
        itemViewHolder.artistName.setText(artistName);

        itemViewHolder.viewParent.setOnClickListener(listener);
        itemViewHolder.title.setOnClickListener(listener);
        itemViewHolder.artistName.setOnClickListener(listener);

        Glide.with(mContext).load(imageUrl).into(itemViewHolder.image);
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {
        View viewParent;
        TextView title;
        TextView artistName;
        ImageView image;

        public ItemViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.track_title);
            artistName = (TextView) itemView.findViewById(R.id.artist_name);
            image = (ImageView) itemView.findViewById(R.id.jacket_image);
            viewParent = itemView.findViewById(R.id.list_item_area);
        }
    }

    public void setListener(OnRecyclerItemClickListener listener) {
        mClickListener = listener;
    }

    public interface OnRecyclerItemClickListener {
        public void onItemClick(View view, Item clicked, int position);
    }
}
