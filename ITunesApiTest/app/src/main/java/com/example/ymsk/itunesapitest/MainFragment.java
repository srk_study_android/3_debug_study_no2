package com.example.ymsk.itunesapitest;

import android.app.LoaderManager;
import android.content.Context;
import android.content.Loader;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.ymsk.itunesapitest.common.AppPreference;
import com.example.ymsk.itunesapitest.common.TextUtil;
import com.example.ymsk.itunesapitest.item.Item;
import com.example.ymsk.itunesapitest.item.ItemLoader;
import com.example.ymsk.itunesapitest.player.PlayControllerFragment;

import java.util.ArrayList;

/**
 * Created by ymsk on 2016/12/14.
 */

public class MainFragment extends android.support.v4.app.Fragment implements
        LoaderManager.LoaderCallbacks,
        View.OnClickListener,
        MusicListAdapter.OnRecyclerItemClickListener {

    public static final String TAG = MainFragment.class.getSimpleName();

    private View mView;
    private ArrayList<Item> mItemList;
    private MusicListAdapter mListAdapter;

    private static final int LOADER_ID_IDLE = 0;
    private static final int LOADER_ID_RUN = 1;
    private int mLoaderID = 0;

    private Spinner mSpinner;
    private EditText mEditText;
    private ProgressBar mProgressBar;
    private Snackbar mSnackBar;

    public MainFragment() {
        super();
    }

    /**
     * Fragmentに表示するビューを生成
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_main, container, false);

        Button searchBtn = (Button) mView.findViewById(R.id.search_button);
        mEditText = (EditText) mView.findViewById(R.id.edit_text);
        mSpinner = (Spinner) mView.findViewById(R.id.spinner);
        mProgressBar = (ProgressBar) mView.findViewById(R.id.progress);

        searchBtn.setOnClickListener(this);
        mEditText.setInputType(InputType.TYPE_CLASS_TEXT);
        mEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                Log.d(TAG, "hasFocus=" + hasFocus);
            }
        });

        createSpinner();
        createSnackBar();

        String keyword = AppPreference.newInstance(getContext()).getLastSearchText();
        if(!keyword.isEmpty()) {
            // 過去に検索済みのアイテムがなかった場合、初回の検索は行わない
            Log.d(TAG, "keyword=" + keyword);
            mEditText.setText(keyword);
            doSearch(keyword);
        } else {
            mEditText.setHint(R.string.error_empty_keyword);
        }
        return mView;
    }

    /**
     * Fragmentに表示していたビューを破棄
     */
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mView = null;
        mItemList = null;
        mListAdapter = null;
    }

    /**
     * 検索Keyword入力ブロックのクリックリスナー
     *
     * @param view
     */
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.search_button:
                final String keyword = mEditText.getText().toString();
                if(keyword.isEmpty()) {
                    Toast.makeText(getContext(), R.string.error_empty_keyword, Toast.LENGTH_SHORT).show();
                } else {
                    AppPreference.newInstance(getContext()).putLastSearchText(keyword);
                    doSearch(keyword);
                }
                break;
            default:
                break;
        }
    }

    /**
     * RecyclerView上のアイテムがクリックされたら呼ばれる
     * -> 音楽再生開始される
     *
     * @param view child view
     * @param clicked child viewの持つItemオブジェクト
     * @param position child viewのポジション
     */
    @Override
    public void onItemClick(View view, Item clicked, int position) {
        switch (view.getId()) {
            case R.id.list_item_area: {
                MainActivity activity = ((MainActivity) getActivity());

                PlayControllerFragment fragment = activity.createPlayerFragment();
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList(PlayControllerFragment.INTENT_PARAM_MUSIC_ITEM, mItemList);
                bundle.putInt(PlayControllerFragment.INTENT_PARAM_CURRENT, position);

                // setArguments(Bundle)を使うとFragmentにパラメータを渡せる
                fragment.setArguments(bundle);
                break;
            }
            case R.id.track_title:
                TextUtil.doClipCopy(getContext(), clicked.getTitle());
                break;
            default:
                break;
        }
    }

    /**
     * ローダーを生成
     */
    @Override
    public Loader onCreateLoader(int id, Bundle bundle) {
        return new ItemLoader(
                getContext(),
                mEditText.getText().toString(),
                AppPreference.newInstance(getContext()).getSearchReq()
        );
    }

    /**
     * ロード完了のコールバック
     */
    @Override
    public void onLoadFinished(Loader loader, Object obj) {
        mProgressBar.setVisibility(View.GONE);
        mSnackBar.dismiss();

        if(mLoaderID == LOADER_ID_IDLE) {
            // AsyncTaskLoaderのコールバックはActivityのライフサイクルに寄り添っており、
            // onStart()のたびにonLoadFinished()が呼ばれる
            // これをそのまま受け入れると、例えばアプリBG -> FG するたびに画面更新が入ってリストの先頭までポジションが戻され、
            // 非常に鬱陶しい
            // 　->　試しにこのif()ブロックをコメントアウトし、
            // 　　　下へスクロールしてからBG -> FG
            // 　　　とすれば、言わんとする"鬱陶しさ"がわかるはず。
            // したがって、実装者の意図した契機以外のonLoadFinished()は受け取らない
            // もといID="RUN"以外のコールバックは無視する
            // 今回のアプリは画面回転や、その他の細かい状態変更は考慮不要のため、この実装でよしとした。
            // やりようはほかにいくらでもあり、この実装が必ずしも正とは限らない。
            return;
        } else {
            mLoaderID = LOADER_ID_IDLE;
        }

        ArrayList<Item> resultSet = (ArrayList<Item>)obj;
        if(resultSet.size() == 0) {
            Log.e(TAG, "resultSet=0, list view NOT update...");
            return;
        }

        mItemList = resultSet;
        mListAdapter = new MusicListAdapter(getActivity(), mItemList);
        mListAdapter.setListener(this);

        RecyclerView recyclerView = (RecyclerView) mView.findViewById(R.id.recycler_view);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(mListAdapter);
    }

    @Override
    public void onLoaderReset(Loader loader) {
        // NO-OP
    }

    /**
     * 次の楽曲へ移動する際に表示するSnackBarの生成
     */
    public void showLoadingSnackBar(String title, String artist) {
        final CoordinatorLayout coordinatorLayout = (CoordinatorLayout) mView.findViewById(R.id.play_controller_view_parent);
        Snackbar snackbar = Snackbar.make(
                coordinatorLayout,
                getString(R.string.snack_bar_change_next_music_alert, title, artist),
                Snackbar.LENGTH_SHORT
        );
        snackbar.show();
    }

    /**
     * 一覧ローディング中を通知するSnackBarの生成
     */
    private void createSnackBar() {
        final CoordinatorLayout coordinatorLayout = (CoordinatorLayout) mView.findViewById(R.id.play_controller_view_parent);
        mSnackBar = Snackbar.make(coordinatorLayout, R.string.snack_bar_loading_alert, Snackbar.LENGTH_INDEFINITE);
    }

    /**
     * Spinnerの生成
     */
    private void createSpinner(){
        Context con = getContext();
        final String[] dataSource = getContext().getResources().getStringArray(R.array.search_order);
        final ArrayAdapter<String> adapter = new ArrayAdapter<>(con, android.R.layout.simple_spinner_item, dataSource);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        mSpinner.setAdapter(adapter);

        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> spinner, View view, int i, long l) {

                String item = (String) spinner.getSelectedItem();

                AppPreference prefs = AppPreference.newInstance(getContext());
                if(item.equals(dataSource[0])) {
                    // すべて
                    prefs.putSearchReq(AppPreference.REQ_TYPE_ALL);
                } else if(item.equals(dataSource[1])) {
                    // アーティスト
                    prefs.putSearchReq(AppPreference.REQ_TYPE_ARTIST);
                } else if(item.equals(dataSource[2])) {
                    // 曲名
                    prefs.putSearchReq(AppPreference.REQ_TYPE_TITLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // アイテムが選択されなかった場合
            }
        });
    }

    /**
     * 検索のロジック
     *
     * @param keyword 検索キーワード
     */
    private void doSearch(String keyword) {
        if(keyword.isEmpty()) {
            mEditText.setHint(R.string.error_empty_keyword);
            return;
        }

        mEditText.setHint(keyword);

        LoaderManager lm = getActivity().getLoaderManager();
        LoaderManager.enableDebugLogging(true); // デバッグ用設定

        mLoaderID = LOADER_ID_RUN;
        lm.restartLoader(mLoaderID, null, this);

        mProgressBar.setVisibility(View.VISIBLE);
        mSnackBar.show();
    }

    private void showKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(mEditText, InputMethodManager.SHOW_IMPLICIT);
        mEditText.requestFocus();
    }

    private void hideKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mEditText.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        mEditText.clearFocus();
    }
}
