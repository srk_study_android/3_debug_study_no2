package com.example.ymsk.itunesapitest.item;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ymsk on 2016/12/14.
 */
public class Item implements Parcelable{

    public Item(String title, String album, String artistName, String imageUrl, String previewUrl){
        mTitle = title;
        mAlbum = album;
        mArtistName = artistName;
        mImageUrl = imageUrl;
        mPreviewUrl = previewUrl;
    }

    public String getTitle() {
        return mTitle;
    }
    public String getAlbum() { return mAlbum; }
    public String getArtistName() {
        return mArtistName;
    }
    public String getImageUrl() {
        return mImageUrl;
    }
    public String getPreviewUrl() {
        return mPreviewUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.mTitle);
        parcel.writeString(this.mAlbum);
        parcel.writeString(this.mArtistName);
        parcel.writeString(this.mImageUrl);
        parcel.writeString(this.mPreviewUrl);
    }

    public Item(Parcel in) {
        mTitle = in.readString();
        mAlbum = in.readString();
        mArtistName = in.readString();
        mImageUrl = in.readString();
        mPreviewUrl = in.readString();
    }

    public static final Creator<Item> CREATOR = new Creator<Item>() {
        public Item createFromParcel(Parcel source) {
            return new Item(source);
        }

        public Item[] newArray(int size) {
            return new Item[size];
        }
    };

    private String mTitle;
    private String mAlbum;
    private String mArtistName;
    private String mImageUrl;
    private String mPreviewUrl;
}
