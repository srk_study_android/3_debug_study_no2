package com.example.ymsk.itunesapitest.item;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;

import com.example.ymsk.itunesapitest.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ymsk on 2016/12/15.
 */

public class ITunesApiAccessor {

    public static final int ERROR_IO = 0x0002;
    public static final int ERROR_NO_RESULT = 0x0003;

    private final String SEARCH_URL;
    private int mStatus = 0;

    public ITunesApiAccessor(String searchUrl) {
        SEARCH_URL = searchUrl;
    }

    /**
     * アイテムリストを返す
     * このメソッドは絶対にNullを返さない
     */
    public List<Item> getItemList(Context context) {
        try {
            final JSONObject jsonObject = getJsonFromApiResult(SEARCH_URL);
            final List<Item> items =  JsonParser.parse(jsonObject);

            if (items.size() == 0) {
                mStatus = ERROR_NO_RESULT;
                throw new ApiAccessException("Api Result None..");
            }

            return items;

        } catch (ApiAccessException e) {
            onApiError(context, mStatus);
            return new ArrayList<>();/* 空リストを返す */
        }
    }

    /**
     * なんらかの理由でAPI結果を取得できなかった場合に投げる例外
     */
    public class ApiAccessException extends Exception {
        public ApiAccessException(String str) {
            super(str);
        }
    }

    /**
     * iTunes Apiを叩いて結果のJSONを取得する
     */
    private JSONObject getJsonFromApiResult(String urlStr) throws ApiAccessException {
        HttpURLConnection con = null;
        JSONObject json = null;
        final byte[] BUFFER = new byte[1024];

        try {
            con = openConnection(urlStr);
            BufferedInputStream bis = new BufferedInputStream(con.getInputStream());
            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            int length;

            while ((length = bis.read(BUFFER)) != -1) {
                if(length > 0) {
                    baos.write(BUFFER, 0, length);
                }
            }

            json = new JSONObject(new String(baos.toByteArray()));

            baos.close();
            bis.close();

        } catch (IOException | JSONException e) {
            mStatus = ERROR_IO;
            throw new ApiAccessException(e.toString());

        } finally {
            if(con != null) {
                closeConnection(con);
            }
        }
        return json;
    }

    /**
     * HTTPコネクションを開始する
     *
     * @param urlStr アクセス先URL
     * @return HttpConnection
     * @throws IOException このメソッドはHTTPの"Close"まで面倒を見ない。したがって「自己責任で使え」と明示している
     */
    private HttpURLConnection openConnection(String urlStr) throws IOException {
        final String REQUEST_METHOD = "GET";
        final int CON_TIME_OUT = 5000;
        final int READ_TIME_OUT = 10000;

        URL url = new URL(urlStr);
        HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();
        httpConnection.setRequestMethod(REQUEST_METHOD);
        httpConnection.setConnectTimeout(CON_TIME_OUT);
        httpConnection.setReadTimeout(READ_TIME_OUT);
        httpConnection.setInstanceFollowRedirects(false);
        httpConnection.setDoInput(true);
        httpConnection.setDoOutput(true);
        httpConnection.connect();

        return httpConnection;
    }

    /**
     * HTTPコネクションを終了する
     *
     * @param connection HttpConnection
     */
    private void closeConnection(HttpURLConnection connection) {
        if(connection != null) {
            connection.disconnect();
        }
    }

    /**
     * エラーハンドリング
     * -> とりあえず取得失敗か取得０件のどちらかで通知。細かくState管理して出し分けてもよい。
     */
    private void onApiError(Context context, int error) {
        switch (error) {
            case ERROR_IO:
                showToast(context, R.string.error_result_io_failed);
                break;
            case ERROR_NO_RESULT:
                showToast(context, R.string.error_result_none);
            default:
                break;
        }
    }

    private void showToast(Context context, final int stringResId) {
        if(context instanceof Activity) {
            final Activity act = (Activity) context;
            act.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(act, stringResId, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}

