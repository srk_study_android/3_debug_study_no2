package com.example.ymsk.itunesapitest.item;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ymsk on 2016/12/15.
 */

public class JsonParser {

    public static List<Item> parse(JSONObject object) {
        List<Item> items = new ArrayList<>();
        int i = 0;
        try {
            JSONArray resultsArray = object.getJSONArray(ItunesApiJson.ARRAY_RESULT);
            final int TAG_LENGTH = resultsArray.length();

            for (i = 0; i < TAG_LENGTH; i++) {
                JSONObject resultObj = resultsArray.getJSONObject(i);

                String title = replaceTextIfNeed(resultObj.getString(ItunesApiJson.TAG_TITLE), ItunesApiJson.TAG_TITLE); // 曲名
                String artist = replaceTextIfNeed(resultObj.getString(ItunesApiJson.TAG_ARTIST), ItunesApiJson.TAG_ARTIST); // アーティスト名
                String album = replaceTextIfNeed(resultObj.getString(ItunesApiJson.TAG_ALBUM), ItunesApiJson.TAG_ALBUM); // アルバム名

                String imageUrl = resultObj.getString(ItunesApiJson.TAG_ALBUM_ART_URL);    // ジャケット画像
                String previewUrl = resultObj.getString(ItunesApiJson.TAG_PREVIEW_URL);   // 視聴ファイルのURL

                if (!imageUrl.isEmpty() && !previewUrl.isEmpty()) {
                    items.add(i, new Item(title, album, artist, imageUrl, previewUrl));
                }
            }

        } catch (JSONException e) {
            Log.e("json error, position=", "i:" + i + " / " + e.toString());
        }

        return items;
    }

    private static String replaceTextIfNeed(String text, String tagName) {
        return text.isEmpty() ? tagName : text;
    }
}
