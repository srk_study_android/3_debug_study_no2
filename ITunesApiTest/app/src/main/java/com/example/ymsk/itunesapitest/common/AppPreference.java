package com.example.ymsk.itunesapitest.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.StringDef;

/**
 * Created by ymsk on 2016/12/19.
 */

public class AppPreference {

    public static final String KEY_APP_PREFS = "key_app_prefs";

    public static final String KEY_SEARCH_TYPE = "key_search_type";

    public static final String KEY_LAST_SEARCH_TEXT = "key_last_search_text";

    @StringDef({REQ_TYPE_ALL, REQ_TYPE_ARTIST, REQ_TYPE_TITLE})
    public @interface REQ_SEARCH{}
    public static final String REQ_TYPE_ALL = "mixTerm";
    public static final String REQ_TYPE_ARTIST = "artistName";
    public static final String REQ_TYPE_TITLE = "titleName";

    public static AppPreference newInstance(Context context) {
        return new AppPreference(context);
    }

    /**
     * 検索オーダーを登録
     */
    public void putSearchReq(@REQ_SEARCH String req) {
        mPrefs.edit().putString(KEY_SEARCH_TYPE, req).apply();
    }

    /**
     * 検索オーダーを取得
     */
    public String getSearchReq() {
        return mPrefs.getString(KEY_SEARCH_TYPE, REQ_TYPE_ALL);
    }

    /**
     * 最後に検索した文字列を保持
     */
    public void putLastSearchText(@NonNull String text) {
        mPrefs.edit().putString(KEY_LAST_SEARCH_TEXT, text).apply();
    }

    /**
     * 最後に検索した文字列を取得
     */
    public String getLastSearchText() {
        return mPrefs.getString(KEY_LAST_SEARCH_TEXT, "");
    }

    private AppPreference(@NonNull Context context) {
        mPrefs = context.getSharedPreferences(KEY_APP_PREFS, Context.MODE_PRIVATE);
    }

    private SharedPreferences mPrefs;
}

